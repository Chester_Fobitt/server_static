'use strict';

var express = require('express'),
	openurl = require('openurl'),
	app = express();

app.use(express.static(__dirname + '/public'));
app.listen(3000);
openurl.open('http://localhost:3000');

console.log('Starting server is localhost:3000');